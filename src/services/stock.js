import {isEmpty, isString} from 'lodash';
import {sum} from '../util';

export default class Stock {
    static validateStock (product) {
        const {name, initialQuantity, purchasedQuantity} = product;

        if (isEmpty(name) || !isString(name)) {
            return false;
        }

        return sum(initialQuantity, purchasedQuantity) >= 50;
        //return !isEmpty(name) && isString(name) && sum(initialQuantity, purchasedQuantity) >= 50;
    }
}
