import {sum} from '../../../src/util';

describe('Util', () => {
    describe('.sum', () => {
        it('should sum two arguments', () => {
            const result = sum(1, 2);
            result.should.equal(3);
        });
    });
});
