const should = require('should');
const sinon = require('sinon');
require('should-sinon');
require('@babel/register')();

process.env.NODE_ENV = 'test';

should.Assertion.alias('threw', 'thrown');

module.exports = {
    should,
    sinon
};
