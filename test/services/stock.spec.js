import {sinon} from '../setup';
import proxyquire from 'proxyquire';

const sumStub = sinon.stub();
const Stock = proxyquire('../../src/services/stock', {
    '../util': {
        sum: sumStub
    }
}).default;

describe('Stock', () => {
    describe('#validateStock', () => {
        afterEach(() => sumStub.resetHistory());

        context('when the product has no name', () => {
            it('should reject the product', () => {
                sumStub.returns(100);

                const bicycle = {};
                const result = Stock.validateStock(bicycle);
        
                result.should.be.false();
            });
        });

        context('when the product has less than 50 units', () => {
            it('should reject the product', () => {
                sumStub.returns(49);

                const car = {
                    name: 'auto'
                };
        
                const result = Stock.validateStock(car);
        
                result.should.be.false();
            });
        });

        context('when the product has 50 or more units', () => {
            context('and it has no name', () => {
                it('should reject the product', () => {
                    sumStub.returns(100);
    
                    const kombi = {};
                    const result = Stock.validateStock(kombi);
            
                    result.should.be.false();
                });
            });

            context('and it has a name defined', () => {
                it('should approve the product', () => {
                    sumStub.returns(100);
    
                    const truck = {
                        name: 'camion'
                    };
            
                    const result = Stock.validateStock(truck);
            
                    result.should.be.true();
                });
            });
        });
    });
});
